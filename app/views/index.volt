<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    {{ get_title() }}
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="{{ url.getBaseUri() }}apple-touch-icon.png">

    <meta name="author" content="">
    <link rel="icon" href="{{ url.getBaseUri() }}favicon.ico">

    {{ stylesheet_link("css/main.css") }}

    {{ javascript_include("js/vendor/modernizr-2.8.3.min.js") }}
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>{{ javascript_include("js/ie8-responsive-file-warning.js") }}<![endif]-->
    {{ javascript_include("js/ie-emulation-modes-warning.js") }}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <div id="wrap">
      <header id="header" role="header">
        <nav class="navbar navbar-inverse">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">Organizyse</a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#contact">Contact</a></li>
              </ul>
            </div><!--/.nav-collapse -->
          </div>
        </nav>
      </header>
      <div id="main" role="main">
        <div class="container">
          <div class="row">
            <aside class="col-lg-4">
            </aside>
            <section class="col-lg-8">
              {{ content() }}
            </section>
          </div>
        </div>
      </div>
    </div>
    <footer id="footer" role="footer">
      <div class="container">
        <p class="text-muted">Organizyse</p>
      </div>
    </footer>
    {{ javascript_include("js/vendor/jquery-1.11.2.min.js") }}
    {{ javascript_include("js/vendor/bootstrap.min.js") }}

    {{ javascript_include("js/plugins.js") }}
    {{ javascript_include("js/main.js") }}

  </body>
</html>