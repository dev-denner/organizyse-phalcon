
{{ form("cliente_fornecedor/create", "method":"post") }}

<table width="100%">
    <tr>
        <td align="left">{{ link_to("cliente_fornecedor", "Go Back") }}</td>
        <td align="right">{{ submit_button("Save") }}</td>
    </tr>
</table>

{{ content() }}

<div align="center">
    <h1>Create cliente_fornecedor</h1>
</div>

<table>
    <tr>
        <td align="right">
            <label for="nome">Nome</label>
        </td>
        <td align="left">
            {{ text_field("nome", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="usercreate">Usercreate</label>
        </td>
        <td align="left">
            {{ text_field("usercreate", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="datecreate">Datecreate</label>
        </td>
        <td align="left">
            {{ text_field("datecreate", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="userupdate">Userupdate</label>
        </td>
        <td align="left">
            {{ text_field("userupdate", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="dateupdate">Dateupdate</label>
        </td>
        <td align="left">
            {{ text_field("dateupdate", "size" : 30) }}
        </td>
    </tr>

    <tr>
        <td></td>
        <td>{{ submit_button("Save") }}</td>
    </tr>
</table>

</form>
