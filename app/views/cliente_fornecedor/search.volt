
{{ content() }}

<table width="100%">
    <tr>
        <td align="left">
            {{ link_to("cliente_fornecedor/index", "Go Back") }}
        </td>
        <td align="right">
            {{ link_to("cliente_fornecedor/new", "Create ") }}
        </td>
    </tr>
</table>

<table class="browse" align="center">
    <thead>
        <tr>
            <th>Id</th>
            <th>Nome</th>
            <th>Usercreate</th>
            <th>Datecreate</th>
            <th>Userupdate</th>
            <th>Dateupdate</th>
         </tr>
    </thead>
    <tbody>
    {% if page.items is defined %}
    {% for cliente_fornecedor in page.items %}
        <tr>
            <td>{{ cliente_fornecedor.getId() }}</td>
            <td>{{ cliente_fornecedor.getNome() }}</td>
            <td>{{ cliente_fornecedor.getUsercreate() }}</td>
            <td>{{ cliente_fornecedor.getDatecreate() }}</td>
            <td>{{ cliente_fornecedor.getUserupdate() }}</td>
            <td>{{ cliente_fornecedor.getDateupdate() }}</td>
            <td>{{ link_to("cliente_fornecedor/edit/"~cliente_fornecedor.getId(), "Edit") }}</td>
            <td>{{ link_to("cliente_fornecedor/delete/"~cliente_fornecedor.getId(), "Delete") }}</td>
        </tr>
    {% endfor %}
    {% endif %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="2" align="right">
                <table align="center">
                    <tr>
                        <td>{{ link_to("cliente_fornecedor/search", "First") }}</td>
                        <td>{{ link_to("cliente_fornecedor/search?page="~page.before, "Previous") }}</td>
                        <td>{{ link_to("cliente_fornecedor/search?page="~page.next, "Next") }}</td>
                        <td>{{ link_to("cliente_fornecedor/search?page="~page.last, "Last") }}</td>
                        <td>{{ page.current~"/"~page.total_pages }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
