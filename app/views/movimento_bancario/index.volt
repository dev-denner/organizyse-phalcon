
{{ content() }}

<div align="right">
    {{ link_to("movimento_bancario/new", "Create movimento_bancario") }}
</div>

{{ form("movimento_bancario/search", "method":"post", "autocomplete" : "off") }}

<div align="center">
    <h1>Search movimento_bancario</h1>
</div>

<table>
    <tr>
        <td align="right">
            <label for="id">Id</label>
        </td>
        <td align="left">
            {{ text_field("id", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="data">Data</label>
        </td>
        <td align="left">
            {{ text_field("data", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="anocomp">Anocomp</label>
        </td>
        <td align="left">
            {{ text_field("anocomp", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="mescomp">Mescomp</label>
        </td>
        <td align="left">
            {{ text_field("mescomp", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="historico">Historico</label>
        </td>
        <td align="left">
            {{ text_field("historico", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="documento">Documento</label>
        </td>
        <td align="left">
            {{ text_field("documento", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="credito">Credito</label>
        </td>
        <td align="left">
            {{ text_field("credito", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="debito">Debito</label>
        </td>
        <td align="left">
            {{ text_field("debito", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="motivo">Motivo</label>
        </td>
        <td align="left">
            {{ text_field("motivo", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="usercreate">Usercreate</label>
        </td>
        <td align="left">
            {{ text_field("usercreate", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="datacreate">Datacreate</label>
        </td>
        <td align="left">
            {{ text_field("datacreate", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="userupdate">Userupdate</label>
        </td>
        <td align="left">
            {{ text_field("userupdate", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="dataupdate">Dataupdate</label>
        </td>
        <td align="left">
            {{ text_field("dataupdate", "size" : 30) }}
        </td>
    </tr>

    <tr>
        <td></td>
        <td>{{ submit_button("Search") }}</td>
    </tr>
</table>

</form>
