
{{ content() }}

<table width="100%">
    <tr>
        <td align="left">
            {{ link_to("movimento_bancario/index", "Go Back") }}
        </td>
        <td align="right">
            {{ link_to("movimento_bancario/new", "Create ") }}
        </td>
    </tr>
</table>

<table class="browse" align="center">
    <thead>
        <tr>
            <th>Id</th>
            <th>Data</th>
            <th>Anocomp</th>
            <th>Mescomp</th>
            <th>Historico</th>
            <th>Documento</th>
            <th>Credito</th>
            <th>Debito</th>
            <th>Motivo</th>
            <th>Usercreate</th>
            <th>Datacreate</th>
            <th>Userupdate</th>
            <th>Dataupdate</th>
         </tr>
    </thead>
    <tbody>
    {% if page.items is defined %}
    {% for movimento_bancario in page.items %}
        <tr>
            <td>{{ movimento_bancario.getId() }}</td>
            <td>{{ movimento_bancario.getData() }}</td>
            <td>{{ movimento_bancario.getAnocomp() }}</td>
            <td>{{ movimento_bancario.getMescomp() }}</td>
            <td>{{ movimento_bancario.getHistorico() }}</td>
            <td>{{ movimento_bancario.getDocumento() }}</td>
            <td>{{ movimento_bancario.getCredito() }}</td>
            <td>{{ movimento_bancario.getDebito() }}</td>
            <td>{{ movimento_bancario.getMotivo() }}</td>
            <td>{{ movimento_bancario.getUsercreate() }}</td>
            <td>{{ movimento_bancario.getDatacreate() }}</td>
            <td>{{ movimento_bancario.getUserupdate() }}</td>
            <td>{{ movimento_bancario.getDataupdate() }}</td>
            <td>{{ link_to("movimento_bancario/edit/"~movimento_bancario.getId(), "Edit") }}</td>
            <td>{{ link_to("movimento_bancario/delete/"~movimento_bancario.getId(), "Delete") }}</td>
        </tr>
    {% endfor %}
    {% endif %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="2" align="right">
                <table align="center">
                    <tr>
                        <td>{{ link_to("movimento_bancario/search", "First") }}</td>
                        <td>{{ link_to("movimento_bancario/search?page="~page.before, "Previous") }}</td>
                        <td>{{ link_to("movimento_bancario/search?page="~page.next, "Next") }}</td>
                        <td>{{ link_to("movimento_bancario/search?page="~page.last, "Last") }}</td>
                        <td>{{ page.current~"/"~page.total_pages }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
