
{{ content() }}

<table width="100%">
    <tr>
        <td align="left">
            {{ link_to("usuarios/index", "Go Back") }}
        </td>
        <td align="right">
            {{ link_to("usuarios/new", "Create ") }}
        </td>
    </tr>
</table>

<table class="browse" align="center">
    <thead>
        <tr>
            <th>Id</th>
            <th>Login</th>
            <th>Senha</th>
            <th>Email</th>
            <th>Nome</th>
         </tr>
    </thead>
    <tbody>
    {% if page.items is defined %}
    {% for usuario in page.items %}
        <tr>
            <td>{{ usuario.getId() }}</td>
            <td>{{ usuario.getLogin() }}</td>
            <td>{{ usuario.getSenha() }}</td>
            <td>{{ usuario.getEmail() }}</td>
            <td>{{ usuario.getNome() }}</td>
            <td>{{ link_to("usuarios/edit/"~usuario.getId(), "Edit") }}</td>
            <td>{{ link_to("usuarios/delete/"~usuario.getId(), "Delete") }}</td>
        </tr>
    {% endfor %}
    {% endif %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="2" align="right">
                <table align="center">
                    <tr>
                        <td>{{ link_to("usuarios/search", "First") }}</td>
                        <td>{{ link_to("usuarios/search?page="~page.before, "Previous") }}</td>
                        <td>{{ link_to("usuarios/search?page="~page.next, "Next") }}</td>
                        <td>{{ link_to("usuarios/search?page="~page.last, "Last") }}</td>
                        <td>{{ page.current~"/"~page.total_pages }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
