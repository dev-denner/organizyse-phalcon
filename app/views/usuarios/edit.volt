{{ content() }}
{{ form("usuarios/save", "method":"post") }}

<table width="100%">
    <tr>
        <td align="left">{{ link_to("usuarios", "Go Back") }}</td>
        <td align="right">{{ submit_button("Save") }}</td>
    </tr>
</table>

<div align="center">
    <h1>Edit usuarios</h1>
</div>

<table>
    <tr>
        <td align="right">
            <label for="login">Login</label>
        </td>
        <td align="left">
            {{ text_field("login", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="senha">Senha</label>
        </td>
        <td align="left">
            {{ text_field("senha", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="email">Email</label>
        </td>
        <td align="left">
            {{ text_field("email", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="nome">Nome</label>
        </td>
        <td align="left">
            {{ text_field("nome", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="status">Status</label>
        </td>
        <td align="left">
                {{ text_field("status") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="token">Token</label>
        </td>
        <td align="left">
            {{ text_field("token", "size" : 30) }}
        </td>
    </tr>

    <tr>
        <td>{{ hidden_field("id") }}</td>
        <td>{{ submit_button("Save") }}</td>
    </tr>
</table>

</form>
