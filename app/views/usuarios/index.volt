
{{ content() }}

<div align="right">
    {{ link_to("usuarios/new", "Create usuarios") }}
</div>

{{ form("usuarios/search", "method":"post", "autocomplete" : "on") }}

<div align="center">
    <h1>Search usuarios</h1>
</div>

<table>
    <tr>
        <td align="right">
            <label for="id">Id</label>
        </td>
        <td align="left">
            {{ text_field("id", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="login">Login</label>
        </td>
        <td align="left">
            {{ text_field("login", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="senha">Senha</label>
        </td>
        <td align="left">
            {{ text_field("senha", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="email">Email</label>
        </td>
        <td align="left">
            {{ text_field("email", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="nome">Nome</label>
        </td>
        <td align="left">
            {{ text_field("nome", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="status">Status</label>
        </td>
        <td align="left">
                {{ text_field("status") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="token">Token</label>
        </td>
        <td align="left">
            {{ text_field("token", "size" : 30) }}
        </td>
    </tr>

    <tr>
        <td></td>
        <td>{{ submit_button("Search") }}</td>
    </tr>
</table>

</form>
