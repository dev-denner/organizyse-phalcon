
{{ content() }}

<table width="100%">
    <tr>
        <td align="left">
            {{ link_to("categorias/index", "Go Back") }}
        </td>
        <td align="right">
            {{ link_to("categorias/new", "Create ") }}
        </td>
    </tr>
</table>

<table class="browse" align="center">
    <thead>
        <tr>
            <th>Id</th>
            <th>Descricao</th>
            <th>Parent</th>
            <th>Usercreate</th>
            <th>Datacreate</th>
            <th>Userupdate</th>
            <th>Dataupdate</th>
         </tr>
    </thead>
    <tbody>
    {% if page.items is defined %}
    {% for categoria in page.items %}
        <tr>
            <td>{{ categoria.getId() }}</td>
            <td>{{ categoria.getDescricao() }}</td>
            <td>{{ categoria.getParent() }}</td>
            <td>{{ categoria.getUsercreate() }}</td>
            <td>{{ categoria.getDatacreate() }}</td>
            <td>{{ categoria.getUserupdate() }}</td>
            <td>{{ categoria.getDataupdate() }}</td>
            <td>{{ link_to("categorias/edit/"~categoria.getId(), "Edit") }}</td>
            <td>{{ link_to("categorias/delete/"~categoria.getId(), "Delete") }}</td>
        </tr>
    {% endfor %}
    {% endif %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="2" align="right">
                <table align="center">
                    <tr>
                        <td>{{ link_to("categorias/search", "First") }}</td>
                        <td>{{ link_to("categorias/search?page="~page.before, "Previous") }}</td>
                        <td>{{ link_to("categorias/search?page="~page.next, "Next") }}</td>
                        <td>{{ link_to("categorias/search?page="~page.last, "Last") }}</td>
                        <td>{{ page.current~"/"~page.total_pages }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
