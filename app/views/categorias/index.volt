
{{ content() }}

<div align="right">
    {{ link_to("categorias/new", "Create categorias") }}
</div>

{{ form("categorias/search", "method":"post", "autocomplete" : "off") }}

<div align="center">
    <h1>Search categorias</h1>
</div>

<table>
    <tr>
        <td align="right">
            <label for="id">Id</label>
        </td>
        <td align="left">
            {{ text_field("id", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="descricao">Descricao</label>
        </td>
        <td align="left">
            {{ text_field("descricao", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="parent">Parent</label>
        </td>
        <td align="left">
            {{ text_field("parent", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="usercreate">Usercreate</label>
        </td>
        <td align="left">
            {{ text_field("usercreate", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="datacreate">Datacreate</label>
        </td>
        <td align="left">
            {{ text_field("datacreate", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="userupdate">Userupdate</label>
        </td>
        <td align="left">
            {{ text_field("userupdate", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="dataupdate">Dataupdate</label>
        </td>
        <td align="left">
            {{ text_field("dataupdate", "size" : 30) }}
        </td>
    </tr>

    <tr>
        <td></td>
        <td>{{ submit_button("Search") }}</td>
    </tr>
</table>

</form>
