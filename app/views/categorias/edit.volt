{{ content() }}
{{ form("categorias/save", "method":"post") }}

<table width="100%">
    <tr>
        <td align="left">{{ link_to("categorias", "Go Back") }}</td>
        <td align="right">{{ submit_button("Save") }}</td>
    </tr>
</table>

<div align="center">
    <h1>Edit categorias</h1>
</div>

<table>
    <tr>
        <td align="right">
            <label for="id">Id</label>
        </td>
        <td align="left">
            {{ text_field("id", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="descricao">Descricao</label>
        </td>
        <td align="left">
            {{ text_field("descricao", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="parent">Parent</label>
        </td>
        <td align="left">
            {{ text_field("parent", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="usercreate">Usercreate</label>
        </td>
        <td align="left">
            {{ text_field("usercreate", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="datacreate">Datacreate</label>
        </td>
        <td align="left">
            {{ text_field("datacreate", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="userupdate">Userupdate</label>
        </td>
        <td align="left">
            {{ text_field("userupdate", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="dataupdate">Dataupdate</label>
        </td>
        <td align="left">
            {{ text_field("dataupdate", "size" : 30) }}
        </td>
    </tr>

    <tr>
        <td>{{ hidden_field("id") }}</td>
        <td>{{ submit_button("Save") }}</td>
    </tr>
</table>

</form>
