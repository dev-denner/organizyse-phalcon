{{ content() }}
{{ form("itens_mov_banc/save", "method":"post") }}

<table width="100%">
    <tr>
        <td align="left">{{ link_to("itens_mov_banc", "Go Back") }}</td>
        <td align="right">{{ submit_button("Save") }}</td>
    </tr>
</table>

<div align="center">
    <h1>Edit itens_mov_banc</h1>
</div>

<table>
    <tr>
        <td align="right">
            <label for="id_mov_banc">Id Of Mov Of Banc</label>
        </td>
        <td align="left">
            {{ text_field("id_mov_banc", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="id_produto">Id Of Produto</label>
        </td>
        <td align="left">
            {{ text_field("id_produto", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="id_marca">Id Of Marca</label>
        </td>
        <td align="left">
            {{ text_field("id_marca", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="qtd">Qtd</label>
        </td>
        <td align="left">
            {{ text_field("qtd", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="valorunit">Valorunit</label>
        </td>
        <td align="left">
            {{ text_field("valorunit", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="valortotal">Valortotal</label>
        </td>
        <td align="left">
            {{ text_field("valortotal", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="id_categoria">Id Of Categoria</label>
        </td>
        <td align="left">
            {{ text_field("id_categoria", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="usercreate">Usercreate</label>
        </td>
        <td align="left">
            {{ text_field("usercreate", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="datecreate">Datecreate</label>
        </td>
        <td align="left">
            {{ text_field("datecreate", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="userupdate">Userupdate</label>
        </td>
        <td align="left">
            {{ text_field("userupdate", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="dateupdate">Dateupdate</label>
        </td>
        <td align="left">
            {{ text_field("dateupdate", "size" : 30) }}
        </td>
    </tr>

    <tr>
        <td>{{ hidden_field("id") }}</td>
        <td>{{ submit_button("Save") }}</td>
    </tr>
</table>

</form>
