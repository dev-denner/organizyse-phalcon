
{{ content() }}

<table width="100%">
    <tr>
        <td align="left">
            {{ link_to("itens_mov_banc/index", "Go Back") }}
        </td>
        <td align="right">
            {{ link_to("itens_mov_banc/new", "Create ") }}
        </td>
    </tr>
</table>

<table class="browse" align="center">
    <thead>
        <tr>
            <th>Id</th>
            <th>Id Of Mov Of Banc</th>
            <th>Id Of Produto</th>
            <th>Id Of Marca</th>
            <th>Qtd</th>
            <th>Valorunit</th>
            <th>Valortotal</th>
            <th>Id Of Categoria</th>
            <th>Usercreate</th>
            <th>Datecreate</th>
            <th>Userupdate</th>
            <th>Dateupdate</th>
         </tr>
    </thead>
    <tbody>
    {% if page.items is defined %}
    {% for itens_mov_banc in page.items %}
        <tr>
            <td>{{ itens_mov_banc.getId() }}</td>
            <td>{{ itens_mov_banc.getIdMovBanc() }}</td>
            <td>{{ itens_mov_banc.getIdProduto() }}</td>
            <td>{{ itens_mov_banc.getIdMarca() }}</td>
            <td>{{ itens_mov_banc.getQtd() }}</td>
            <td>{{ itens_mov_banc.getValorunit() }}</td>
            <td>{{ itens_mov_banc.getValortotal() }}</td>
            <td>{{ itens_mov_banc.getIdCategoria() }}</td>
            <td>{{ itens_mov_banc.getUsercreate() }}</td>
            <td>{{ itens_mov_banc.getDatecreate() }}</td>
            <td>{{ itens_mov_banc.getUserupdate() }}</td>
            <td>{{ itens_mov_banc.getDateupdate() }}</td>
            <td>{{ link_to("itens_mov_banc/edit/"~itens_mov_banc.getId(), "Edit") }}</td>
            <td>{{ link_to("itens_mov_banc/delete/"~itens_mov_banc.getId(), "Delete") }}</td>
        </tr>
    {% endfor %}
    {% endif %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="2" align="right">
                <table align="center">
                    <tr>
                        <td>{{ link_to("itens_mov_banc/search", "First") }}</td>
                        <td>{{ link_to("itens_mov_banc/search?page="~page.before, "Previous") }}</td>
                        <td>{{ link_to("itens_mov_banc/search?page="~page.next, "Next") }}</td>
                        <td>{{ link_to("itens_mov_banc/search?page="~page.last, "Last") }}</td>
                        <td>{{ page.current~"/"~page.total_pages }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
