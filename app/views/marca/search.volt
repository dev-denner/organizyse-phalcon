
{{ content() }}

<table width="100%">
    <tr>
        <td align="left">
            {{ link_to("marca/index", "Go Back") }}
        </td>
        <td align="right">
            {{ link_to("marca/new", "Create ") }}
        </td>
    </tr>
</table>

<table class="browse" align="center">
    <thead>
        <tr>
            <th>Id</th>
            <th>Nome</th>
            <th>Usercreate</th>
            <th>Datecreate</th>
            <th>Userupdate</th>
            <th>Dateupdate</th>
         </tr>
    </thead>
    <tbody>
    {% if page.items is defined %}
    {% for marca in page.items %}
        <tr>
            <td>{{ marca.getId() }}</td>
            <td>{{ marca.getNome() }}</td>
            <td>{{ marca.getUsercreate() }}</td>
            <td>{{ marca.getDatecreate() }}</td>
            <td>{{ marca.getUserupdate() }}</td>
            <td>{{ marca.getDateupdate() }}</td>
            <td>{{ link_to("marca/edit/"~marca.getId(), "Edit") }}</td>
            <td>{{ link_to("marca/delete/"~marca.getId(), "Delete") }}</td>
        </tr>
    {% endfor %}
    {% endif %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="2" align="right">
                <table align="center">
                    <tr>
                        <td>{{ link_to("marca/search", "First") }}</td>
                        <td>{{ link_to("marca/search?page="~page.before, "Previous") }}</td>
                        <td>{{ link_to("marca/search?page="~page.next, "Next") }}</td>
                        <td>{{ link_to("marca/search?page="~page.last, "Last") }}</td>
                        <td>{{ page.current~"/"~page.total_pages }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
