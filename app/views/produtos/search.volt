
{{ content() }}

<table width="100%">
    <tr>
        <td align="left">
            {{ link_to("produtos/index", "Go Back") }}
        </td>
        <td align="right">
            {{ link_to("produtos/new", "Create ") }}
        </td>
    </tr>
</table>

<table class="browse" align="center">
    <thead>
        <tr>
            <th>Id</th>
            <th>Descricao</th>
            <th>Unid</th>
            <th>Valormedio</th>
            <th>Usercreate</th>
            <th>Datecreate</th>
            <th>Userupdate</th>
            <th>Dateupdate</th>
         </tr>
    </thead>
    <tbody>
    {% if page.items is defined %}
    {% for produto in page.items %}
        <tr>
            <td>{{ produto.getId() }}</td>
            <td>{{ produto.getDescricao() }}</td>
            <td>{{ produto.getUnid() }}</td>
            <td>{{ produto.getValormedio() }}</td>
            <td>{{ produto.getUsercreate() }}</td>
            <td>{{ produto.getDatecreate() }}</td>
            <td>{{ produto.getUserupdate() }}</td>
            <td>{{ produto.getDateupdate() }}</td>
            <td>{{ link_to("produtos/edit/"~produto.getId(), "Edit") }}</td>
            <td>{{ link_to("produtos/delete/"~produto.getId(), "Delete") }}</td>
        </tr>
    {% endfor %}
    {% endif %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="2" align="right">
                <table align="center">
                    <tr>
                        <td>{{ link_to("produtos/search", "First") }}</td>
                        <td>{{ link_to("produtos/search?page="~page.before, "Previous") }}</td>
                        <td>{{ link_to("produtos/search?page="~page.next, "Next") }}</td>
                        <td>{{ link_to("produtos/search?page="~page.last, "Last") }}</td>
                        <td>{{ page.current~"/"~page.total_pages }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
