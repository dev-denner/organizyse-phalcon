
{{ form("produtos/create", "method":"post") }}

<table width="100%">
    <tr>
        <td align="left">{{ link_to("produtos", "Go Back") }}</td>
        <td align="right">{{ submit_button("Save") }}</td>
    </tr>
</table>

{{ content() }}

<div align="center">
    <h1>Create produtos</h1>
</div>

<table>
    <tr>
        <td align="right">
            <label for="descricao">Descricao</label>
        </td>
        <td align="left">
            {{ text_field("descricao", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="unid">Unid</label>
        </td>
        <td align="left">
            {{ text_field("unid", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="valormedio">Valormedio</label>
        </td>
        <td align="left">
            {{ text_field("valormedio", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="usercreate">Usercreate</label>
        </td>
        <td align="left">
            {{ text_field("usercreate", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="datecreate">Datecreate</label>
        </td>
        <td align="left">
            {{ text_field("datecreate", "size" : 30) }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="userupdate">Userupdate</label>
        </td>
        <td align="left">
            {{ text_field("userupdate", "type" : "numeric") }}
        </td>
    </tr>
    <tr>
        <td align="right">
            <label for="dateupdate">Dateupdate</label>
        </td>
        <td align="left">
            {{ text_field("dateupdate", "size" : 30) }}
        </td>
    </tr>

    <tr>
        <td></td>
        <td>{{ submit_button("Save") }}</td>
    </tr>
</table>

</form>
