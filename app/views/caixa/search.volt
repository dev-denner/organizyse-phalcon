
{{ content() }}

<table width="100%">
    <tr>
        <td align="left">
            {{ link_to("caixa/index", "Go Back") }}
        </td>
        <td align="right">
            {{ link_to("caixa/new", "Create ") }}
        </td>
    </tr>
</table>

<table class="browse" align="center">
    <thead>
        <tr>
            <th>Id</th>
            <th>Descricao</th>
            <th>Saldo</th>
            <th>Usercreate</th>
            <th>Datecreate</th>
            <th>Userupdate</th>
            <th>Dateupdate</th>
         </tr>
    </thead>
    <tbody>
    {% if page.items is defined %}
    {% for caixa in page.items %}
        <tr>
            <td>{{ caixa.getId() }}</td>
            <td>{{ caixa.getDescricao() }}</td>
            <td>{{ caixa.getSaldo() }}</td>
            <td>{{ caixa.getUsercreate() }}</td>
            <td>{{ caixa.getDatecreate() }}</td>
            <td>{{ caixa.getUserupdate() }}</td>
            <td>{{ caixa.getDateupdate() }}</td>
            <td>{{ link_to("caixa/edit/"~caixa.getId(), "Edit") }}</td>
            <td>{{ link_to("caixa/delete/"~caixa.getId(), "Delete") }}</td>
        </tr>
    {% endfor %}
    {% endif %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="2" align="right">
                <table align="center">
                    <tr>
                        <td>{{ link_to("caixa/search", "First") }}</td>
                        <td>{{ link_to("caixa/search?page="~page.before, "Previous") }}</td>
                        <td>{{ link_to("caixa/search?page="~page.next, "Next") }}</td>
                        <td>{{ link_to("caixa/search?page="~page.last, "Last") }}</td>
                        <td>{{ page.current~"/"~page.total_pages }}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
