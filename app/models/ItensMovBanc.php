<?php

class ItensMovBanc extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $id_mov_banc;

    /**
     *
     * @var integer
     */
    protected $id_produto;

    /**
     *
     * @var integer
     */
    protected $id_marca;

    /**
     *
     * @var string
     */
    protected $qtd;

    /**
     *
     * @var string
     */
    protected $valorunit;

    /**
     *
     * @var string
     */
    protected $valortotal;

    /**
     *
     * @var integer
     */
    protected $id_categoria;

    /**
     *
     * @var integer
     */
    protected $usercreate;

    /**
     *
     * @var string
     */
    protected $datecreate;

    /**
     *
     * @var integer
     */
    protected $userupdate;

    /**
     *
     * @var string
     */
    protected $dateupdate;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field id_mov_banc
     *
     * @param integer $id_mov_banc
     * @return $this
     */
    public function setIdMovBanc($id_mov_banc)
    {
        $this->id_mov_banc = $id_mov_banc;

        return $this;
    }

    /**
     * Method to set the value of field id_produto
     *
     * @param integer $id_produto
     * @return $this
     */
    public function setIdProduto($id_produto)
    {
        $this->id_produto = $id_produto;

        return $this;
    }

    /**
     * Method to set the value of field id_marca
     *
     * @param integer $id_marca
     * @return $this
     */
    public function setIdMarca($id_marca)
    {
        $this->id_marca = $id_marca;

        return $this;
    }

    /**
     * Method to set the value of field qtd
     *
     * @param string $qtd
     * @return $this
     */
    public function setQtd($qtd)
    {
        $this->qtd = $qtd;

        return $this;
    }

    /**
     * Method to set the value of field valorunit
     *
     * @param string $valorunit
     * @return $this
     */
    public function setValorunit($valorunit)
    {
        $this->valorunit = $valorunit;

        return $this;
    }

    /**
     * Method to set the value of field valortotal
     *
     * @param string $valortotal
     * @return $this
     */
    public function setValortotal($valortotal)
    {
        $this->valortotal = $valortotal;

        return $this;
    }

    /**
     * Method to set the value of field id_categoria
     *
     * @param integer $id_categoria
     * @return $this
     */
    public function setIdCategoria($id_categoria)
    {
        $this->id_categoria = $id_categoria;

        return $this;
    }

    /**
     * Method to set the value of field usercreate
     *
     * @param integer $usercreate
     * @return $this
     */
    public function setUsercreate($usercreate)
    {
        $this->usercreate = $usercreate;

        return $this;
    }

    /**
     * Method to set the value of field datecreate
     *
     * @param string $datecreate
     * @return $this
     */
    public function setDatecreate($datecreate)
    {
        $this->datecreate = $datecreate;

        return $this;
    }

    /**
     * Method to set the value of field userupdate
     *
     * @param integer $userupdate
     * @return $this
     */
    public function setUserupdate($userupdate)
    {
        $this->userupdate = $userupdate;

        return $this;
    }

    /**
     * Method to set the value of field dateupdate
     *
     * @param string $dateupdate
     * @return $this
     */
    public function setDateupdate($dateupdate)
    {
        $this->dateupdate = $dateupdate;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field id_mov_banc
     *
     * @return integer
     */
    public function getIdMovBanc()
    {
        return $this->id_mov_banc;
    }

    /**
     * Returns the value of field id_produto
     *
     * @return integer
     */
    public function getIdProduto()
    {
        return $this->id_produto;
    }

    /**
     * Returns the value of field id_marca
     *
     * @return integer
     */
    public function getIdMarca()
    {
        return $this->id_marca;
    }

    /**
     * Returns the value of field qtd
     *
     * @return string
     */
    public function getQtd()
    {
        return $this->qtd;
    }

    /**
     * Returns the value of field valorunit
     *
     * @return string
     */
    public function getValorunit()
    {
        return $this->valorunit;
    }

    /**
     * Returns the value of field valortotal
     *
     * @return string
     */
    public function getValortotal()
    {
        return $this->valortotal;
    }

    /**
     * Returns the value of field id_categoria
     *
     * @return integer
     */
    public function getIdCategoria()
    {
        return $this->id_categoria;
    }

    /**
     * Returns the value of field usercreate
     *
     * @return integer
     */
    public function getUsercreate()
    {
        return $this->usercreate;
    }

    /**
     * Returns the value of field datecreate
     *
     * @return string
     */
    public function getDatecreate()
    {
        return $this->datecreate;
    }

    /**
     * Returns the value of field userupdate
     *
     * @return integer
     */
    public function getUserupdate()
    {
        return $this->userupdate;
    }

    /**
     * Returns the value of field dateupdate
     *
     * @return string
     */
    public function getDateupdate()
    {
        return $this->dateupdate;
    }

}
