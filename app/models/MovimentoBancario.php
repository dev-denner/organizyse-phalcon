<?php

class MovimentoBancario extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $data;

    /**
     *
     * @var integer
     */
    protected $anocomp;

    /**
     *
     * @var integer
     */
    protected $mescomp;

    /**
     *
     * @var string
     */
    protected $historico;

    /**
     *
     * @var string
     */
    protected $documento;

    /**
     *
     * @var string
     */
    protected $credito;

    /**
     *
     * @var string
     */
    protected $debito;

    /**
     *
     * @var string
     */
    protected $motivo;

    /**
     *
     * @var integer
     */
    protected $usercreate;

    /**
     *
     * @var string
     */
    protected $datacreate;

    /**
     *
     * @var integer
     */
    protected $userupdate;

    /**
     *
     * @var string
     */
    protected $dataupdate;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field data
     *
     * @param string $data
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Method to set the value of field anocomp
     *
     * @param integer $anocomp
     * @return $this
     */
    public function setAnocomp($anocomp)
    {
        $this->anocomp = $anocomp;

        return $this;
    }

    /**
     * Method to set the value of field mescomp
     *
     * @param integer $mescomp
     * @return $this
     */
    public function setMescomp($mescomp)
    {
        $this->mescomp = $mescomp;

        return $this;
    }

    /**
     * Method to set the value of field historico
     *
     * @param string $historico
     * @return $this
     */
    public function setHistorico($historico)
    {
        $this->historico = $historico;

        return $this;
    }

    /**
     * Method to set the value of field documento
     *
     * @param string $documento
     * @return $this
     */
    public function setDocumento($documento)
    {
        $this->documento = $documento;

        return $this;
    }

    /**
     * Method to set the value of field credito
     *
     * @param string $credito
     * @return $this
     */
    public function setCredito($credito)
    {
        $this->credito = $credito;

        return $this;
    }

    /**
     * Method to set the value of field debito
     *
     * @param string $debito
     * @return $this
     */
    public function setDebito($debito)
    {
        $this->debito = $debito;

        return $this;
    }

    /**
     * Method to set the value of field motivo
     *
     * @param string $motivo
     * @return $this
     */
    public function setMotivo($motivo)
    {
        $this->motivo = $motivo;

        return $this;
    }

    /**
     * Method to set the value of field usercreate
     *
     * @param integer $usercreate
     * @return $this
     */
    public function setUsercreate($usercreate)
    {
        $this->usercreate = $usercreate;

        return $this;
    }

    /**
     * Method to set the value of field datacreate
     *
     * @param string $datacreate
     * @return $this
     */
    public function setDatacreate($datacreate)
    {
        $this->datacreate = $datacreate;

        return $this;
    }

    /**
     * Method to set the value of field userupdate
     *
     * @param integer $userupdate
     * @return $this
     */
    public function setUserupdate($userupdate)
    {
        $this->userupdate = $userupdate;

        return $this;
    }

    /**
     * Method to set the value of field dataupdate
     *
     * @param string $dataupdate
     * @return $this
     */
    public function setDataupdate($dataupdate)
    {
        $this->dataupdate = $dataupdate;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Returns the value of field anocomp
     *
     * @return integer
     */
    public function getAnocomp()
    {
        return $this->anocomp;
    }

    /**
     * Returns the value of field mescomp
     *
     * @return integer
     */
    public function getMescomp()
    {
        return $this->mescomp;
    }

    /**
     * Returns the value of field historico
     *
     * @return string
     */
    public function getHistorico()
    {
        return $this->historico;
    }

    /**
     * Returns the value of field documento
     *
     * @return string
     */
    public function getDocumento()
    {
        return $this->documento;
    }

    /**
     * Returns the value of field credito
     *
     * @return string
     */
    public function getCredito()
    {
        return $this->credito;
    }

    /**
     * Returns the value of field debito
     *
     * @return string
     */
    public function getDebito()
    {
        return $this->debito;
    }

    /**
     * Returns the value of field motivo
     *
     * @return string
     */
    public function getMotivo()
    {
        return $this->motivo;
    }

    /**
     * Returns the value of field usercreate
     *
     * @return integer
     */
    public function getUsercreate()
    {
        return $this->usercreate;
    }

    /**
     * Returns the value of field datacreate
     *
     * @return string
     */
    public function getDatacreate()
    {
        return $this->datacreate;
    }

    /**
     * Returns the value of field userupdate
     *
     * @return integer
     */
    public function getUserupdate()
    {
        return $this->userupdate;
    }

    /**
     * Returns the value of field dataupdate
     *
     * @return string
     */
    public function getDataupdate()
    {
        return $this->dataupdate;
    }

}
