<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

class MovimentoBancarioMigration_1001 extends Migration
{

    public function up()
    {
        $this->morphTable(
            'movimento_bancario',
            array(
            'columns' => array(
                new Column(
                    'id',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'unsigned' => true,
                        'notNull' => true,
                        'autoIncrement' => true,
                        'size' => 11,
                        'first' => true
                    )
                ),
                new Column(
                    'data',
                    array(
                        'type' => Column::TYPE_DATETIME,
                        'notNull' => true,
                        'size' => 1,
                        'after' => 'id'
                    )
                ),
                new Column(
                    'anocomp',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'unsigned' => true,
                        'notNull' => true,
                        'size' => 4,
                        'after' => 'data'
                    )
                ),
                new Column(
                    'mescomp',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'unsigned' => true,
                        'notNull' => true,
                        'size' => 2,
                        'after' => 'anocomp'
                    )
                ),
                new Column(
                    'historico',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'size' => 150,
                        'after' => 'mescomp'
                    )
                ),
                new Column(
                    'documento',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'size' => 45,
                        'after' => 'historico'
                    )
                ),
                new Column(
                    'credito',
                    array(
                        'type' => Column::TYPE_DOUBLE,
                        'size' => 1,
                        'after' => 'documento'
                    )
                ),
                new Column(
                    'debito',
                    array(
                        'type' => Column::TYPE_DOUBLE,
                        'size' => 1,
                        'after' => 'credito'
                    )
                ),
                new Column(
                    'motivo',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'size' => 255,
                        'after' => 'debito'
                    )
                ),
                new Column(
                    'usercreate',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'unsigned' => true,
                        'size' => 10,
                        'after' => 'motivo'
                    )
                ),
                new Column(
                    'datacreate',
                    array(
                        'type' => Column::TYPE_DATETIME,
                        'size' => 1,
                        'after' => 'usercreate'
                    )
                ),
                new Column(
                    'userupdate',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'unsigned' => true,
                        'size' => 10,
                        'after' => 'datacreate'
                    )
                ),
                new Column(
                    'dataupdate',
                    array(
                        'type' => Column::TYPE_DATETIME,
                        'size' => 1,
                        'after' => 'userupdate'
                    )
                )
            ),
            'indexes' => array(
                new Index('PRIMARY', array('id')),
                new Index('movbanc_idx1', array('anocomp', 'mescomp'))
            ),
            'options' => array(
                'TABLE_TYPE' => 'BASE TABLE',
                'AUTO_INCREMENT' => '1',
                'ENGINE' => 'InnoDB',
                'TABLE_COLLATION' => 'latin1_swedish_ci'
            )
        )
        );
    }
}
