<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

class ItensMovBancMigration_1001 extends Migration
{

    public function up()
    {
        $this->morphTable(
            'itens_mov_banc',
            array(
            'columns' => array(
                new Column(
                    'id',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'unsigned' => true,
                        'notNull' => true,
                        'autoIncrement' => true,
                        'size' => 10,
                        'first' => true
                    )
                ),
                new Column(
                    'id_mov_banc',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'unsigned' => true,
                        'notNull' => true,
                        'size' => 10,
                        'after' => 'id'
                    )
                ),
                new Column(
                    'id_produto',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'unsigned' => true,
                        'notNull' => true,
                        'size' => 10,
                        'after' => 'id_mov_banc'
                    )
                ),
                new Column(
                    'id_marca',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'unsigned' => true,
                        'size' => 10,
                        'after' => 'id_produto'
                    )
                ),
                new Column(
                    'qtd',
                    array(
                        'type' => Column::TYPE_DOUBLE,
                        'size' => 1,
                        'after' => 'id_marca'
                    )
                ),
                new Column(
                    'valorunit',
                    array(
                        'type' => Column::TYPE_DOUBLE,
                        'size' => 1,
                        'after' => 'qtd'
                    )
                ),
                new Column(
                    'valortotal',
                    array(
                        'type' => Column::TYPE_DOUBLE,
                        'size' => 1,
                        'after' => 'valorunit'
                    )
                ),
                new Column(
                    'id_categoria',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'unsigned' => true,
                        'size' => 10,
                        'after' => 'valortotal'
                    )
                ),
                new Column(
                    'usercreate',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'unsigned' => true,
                        'size' => 10,
                        'after' => 'id_categoria'
                    )
                ),
                new Column(
                    'datecreate',
                    array(
                        'type' => Column::TYPE_DATETIME,
                        'size' => 1,
                        'after' => 'usercreate'
                    )
                ),
                new Column(
                    'userupdate',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'unsigned' => true,
                        'size' => 10,
                        'after' => 'datecreate'
                    )
                ),
                new Column(
                    'dateupdate',
                    array(
                        'type' => Column::TYPE_DATETIME,
                        'size' => 1,
                        'after' => 'userupdate'
                    )
                )
            ),
            'indexes' => array(
                new Index('PRIMARY', array('id')),
                new Index('itensmovbanc_movbanc_idx', array('id_mov_banc')),
                new Index('itensmovbanc_produto_idx', array('id_produto')),
                new Index('itensmovbanc_categorias_idx', array('id_categoria')),
                new Index('itensmovbanc_idx', array('id_mov_banc', 'id_produto'))
            ),
            'references' => array(
                new Reference('itensmovbanc_categorias', array(
                    'referencedSchema' => 'erp',
                    'referencedTable' => 'categorias',
                    'columns' => array('id_categoria'),
                    'referencedColumns' => array('id')
                )),
                new Reference('itensmovbanc_movbanc', array(
                    'referencedSchema' => 'erp',
                    'referencedTable' => 'movimento_bancario',
                    'columns' => array('id_mov_banc'),
                    'referencedColumns' => array('id')
                )),
                new Reference('itensmovbanc_produto', array(
                    'referencedSchema' => 'erp',
                    'referencedTable' => 'produtos',
                    'columns' => array('id_produto'),
                    'referencedColumns' => array('id')
                ))
            ),
            'options' => array(
                'TABLE_TYPE' => 'BASE TABLE',
                'AUTO_INCREMENT' => '1',
                'ENGINE' => 'InnoDB',
                'TABLE_COLLATION' => 'latin1_swedish_ci'
            )
        )
        );
    }
}
