<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class ItensMovBancController extends ControllerBase
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for itens_mov_banc
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "ItensMovBanc", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id";

        $itens_mov_banc = ItensMovBanc::find($parameters);
        if (count($itens_mov_banc) == 0) {
            $this->flash->notice("The search did not find any itens_mov_banc");

            return $this->dispatcher->forward(array(
                "controller" => "itens_mov_banc",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $itens_mov_banc,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a itens_mov_banc
     *
     * @param string $id
     */
    public function editAction($id)
    {

        if (!$this->request->isPost()) {

            $itens_mov_banc = ItensMovBanc::findFirstByid($id);
            if (!$itens_mov_banc) {
                $this->flash->error("itens_mov_banc was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "itens_mov_banc",
                    "action" => "index"
                ));
            }

            $this->view->id = $itens_mov_banc->id;

            $this->tag->setDefault("id", $itens_mov_banc->getId());
            $this->tag->setDefault("id_mov_banc", $itens_mov_banc->getIdMovBanc());
            $this->tag->setDefault("id_produto", $itens_mov_banc->getIdProduto());
            $this->tag->setDefault("id_marca", $itens_mov_banc->getIdMarca());
            $this->tag->setDefault("qtd", $itens_mov_banc->getQtd());
            $this->tag->setDefault("valorunit", $itens_mov_banc->getValorunit());
            $this->tag->setDefault("valortotal", $itens_mov_banc->getValortotal());
            $this->tag->setDefault("id_categoria", $itens_mov_banc->getIdCategoria());
            $this->tag->setDefault("usercreate", $itens_mov_banc->getUsercreate());
            $this->tag->setDefault("datecreate", $itens_mov_banc->getDatecreate());
            $this->tag->setDefault("userupdate", $itens_mov_banc->getUserupdate());
            $this->tag->setDefault("dateupdate", $itens_mov_banc->getDateupdate());
            
        }
    }

    /**
     * Creates a new itens_mov_banc
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "itens_mov_banc",
                "action" => "index"
            ));
        }

        $itens_mov_banc = new ItensMovBanc();

        $itens_mov_banc->setIdMovBanc($this->request->getPost("id_mov_banc"));
        $itens_mov_banc->setIdProduto($this->request->getPost("id_produto"));
        $itens_mov_banc->setIdMarca($this->request->getPost("id_marca"));
        $itens_mov_banc->setQtd($this->request->getPost("qtd"));
        $itens_mov_banc->setValorunit($this->request->getPost("valorunit"));
        $itens_mov_banc->setValortotal($this->request->getPost("valortotal"));
        $itens_mov_banc->setIdCategoria($this->request->getPost("id_categoria"));
        $itens_mov_banc->setUsercreate($this->request->getPost("usercreate"));
        $itens_mov_banc->setDatecreate($this->request->getPost("datecreate"));
        $itens_mov_banc->setUserupdate($this->request->getPost("userupdate"));
        $itens_mov_banc->setDateupdate($this->request->getPost("dateupdate"));
        

        if (!$itens_mov_banc->save()) {
            foreach ($itens_mov_banc->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "itens_mov_banc",
                "action" => "new"
            ));
        }

        $this->flash->success("itens_mov_banc was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "itens_mov_banc",
            "action" => "index"
        ));

    }

    /**
     * Saves a itens_mov_banc edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "itens_mov_banc",
                "action" => "index"
            ));
        }

        $id = $this->request->getPost("id");

        $itens_mov_banc = ItensMovBanc::findFirstByid($id);
        if (!$itens_mov_banc) {
            $this->flash->error("itens_mov_banc does not exist " . $id);

            return $this->dispatcher->forward(array(
                "controller" => "itens_mov_banc",
                "action" => "index"
            ));
        }

        $itens_mov_banc->setIdMovBanc($this->request->getPost("id_mov_banc"));
        $itens_mov_banc->setIdProduto($this->request->getPost("id_produto"));
        $itens_mov_banc->setIdMarca($this->request->getPost("id_marca"));
        $itens_mov_banc->setQtd($this->request->getPost("qtd"));
        $itens_mov_banc->setValorunit($this->request->getPost("valorunit"));
        $itens_mov_banc->setValortotal($this->request->getPost("valortotal"));
        $itens_mov_banc->setIdCategoria($this->request->getPost("id_categoria"));
        $itens_mov_banc->setUsercreate($this->request->getPost("usercreate"));
        $itens_mov_banc->setDatecreate($this->request->getPost("datecreate"));
        $itens_mov_banc->setUserupdate($this->request->getPost("userupdate"));
        $itens_mov_banc->setDateupdate($this->request->getPost("dateupdate"));
        

        if (!$itens_mov_banc->save()) {

            foreach ($itens_mov_banc->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "itens_mov_banc",
                "action" => "edit",
                "params" => array($itens_mov_banc->id)
            ));
        }

        $this->flash->success("itens_mov_banc was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "itens_mov_banc",
            "action" => "index"
        ));

    }

    /**
     * Deletes a itens_mov_banc
     *
     * @param string $id
     */
    public function deleteAction($id)
    {

        $itens_mov_banc = ItensMovBanc::findFirstByid($id);
        if (!$itens_mov_banc) {
            $this->flash->error("itens_mov_banc was not found");

            return $this->dispatcher->forward(array(
                "controller" => "itens_mov_banc",
                "action" => "index"
            ));
        }

        if (!$itens_mov_banc->delete()) {

            foreach ($itens_mov_banc->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "itens_mov_banc",
                "action" => "search"
            ));
        }

        $this->flash->success("itens_mov_banc was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "itens_mov_banc",
            "action" => "index"
        ));
    }

}
