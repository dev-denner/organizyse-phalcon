<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class ProdutosController extends ControllerBase
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for produtos
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Produtos", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id";

        $produtos = Produtos::find($parameters);
        if (count($produtos) == 0) {
            $this->flash->notice("The search did not find any produtos");

            return $this->dispatcher->forward(array(
                "controller" => "produtos",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $produtos,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a produto
     *
     * @param string $id
     */
    public function editAction($id)
    {

        if (!$this->request->isPost()) {

            $produto = Produtos::findFirstByid($id);
            if (!$produto) {
                $this->flash->error("produto was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "produtos",
                    "action" => "index"
                ));
            }

            $this->view->id = $produto->id;

            $this->tag->setDefault("id", $produto->getId());
            $this->tag->setDefault("descricao", $produto->getDescricao());
            $this->tag->setDefault("unid", $produto->getUnid());
            $this->tag->setDefault("valormedio", $produto->getValormedio());
            $this->tag->setDefault("usercreate", $produto->getUsercreate());
            $this->tag->setDefault("datecreate", $produto->getDatecreate());
            $this->tag->setDefault("userupdate", $produto->getUserupdate());
            $this->tag->setDefault("dateupdate", $produto->getDateupdate());
            
        }
    }

    /**
     * Creates a new produto
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "produtos",
                "action" => "index"
            ));
        }

        $produto = new Produtos();

        $produto->setDescricao($this->request->getPost("descricao"));
        $produto->setUnid($this->request->getPost("unid"));
        $produto->setValormedio($this->request->getPost("valormedio"));
        $produto->setUsercreate($this->request->getPost("usercreate"));
        $produto->setDatecreate($this->request->getPost("datecreate"));
        $produto->setUserupdate($this->request->getPost("userupdate"));
        $produto->setDateupdate($this->request->getPost("dateupdate"));
        

        if (!$produto->save()) {
            foreach ($produto->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "produtos",
                "action" => "new"
            ));
        }

        $this->flash->success("produto was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "produtos",
            "action" => "index"
        ));

    }

    /**
     * Saves a produto edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "produtos",
                "action" => "index"
            ));
        }

        $id = $this->request->getPost("id");

        $produto = Produtos::findFirstByid($id);
        if (!$produto) {
            $this->flash->error("produto does not exist " . $id);

            return $this->dispatcher->forward(array(
                "controller" => "produtos",
                "action" => "index"
            ));
        }

        $produto->setDescricao($this->request->getPost("descricao"));
        $produto->setUnid($this->request->getPost("unid"));
        $produto->setValormedio($this->request->getPost("valormedio"));
        $produto->setUsercreate($this->request->getPost("usercreate"));
        $produto->setDatecreate($this->request->getPost("datecreate"));
        $produto->setUserupdate($this->request->getPost("userupdate"));
        $produto->setDateupdate($this->request->getPost("dateupdate"));
        

        if (!$produto->save()) {

            foreach ($produto->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "produtos",
                "action" => "edit",
                "params" => array($produto->id)
            ));
        }

        $this->flash->success("produto was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "produtos",
            "action" => "index"
        ));

    }

    /**
     * Deletes a produto
     *
     * @param string $id
     */
    public function deleteAction($id)
    {

        $produto = Produtos::findFirstByid($id);
        if (!$produto) {
            $this->flash->error("produto was not found");

            return $this->dispatcher->forward(array(
                "controller" => "produtos",
                "action" => "index"
            ));
        }

        if (!$produto->delete()) {

            foreach ($produto->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "produtos",
                "action" => "search"
            ));
        }

        $this->flash->success("produto was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "produtos",
            "action" => "index"
        ));
    }

}
