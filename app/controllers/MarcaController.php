<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class MarcaController extends ControllerBase
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for marca
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Marca", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id";

        $marca = Marca::find($parameters);
        if (count($marca) == 0) {
            $this->flash->notice("The search did not find any marca");

            return $this->dispatcher->forward(array(
                "controller" => "marca",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $marca,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a marca
     *
     * @param string $id
     */
    public function editAction($id)
    {

        if (!$this->request->isPost()) {

            $marca = Marca::findFirstByid($id);
            if (!$marca) {
                $this->flash->error("marca was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "marca",
                    "action" => "index"
                ));
            }

            $this->view->id = $marca->id;

            $this->tag->setDefault("id", $marca->getId());
            $this->tag->setDefault("nome", $marca->getNome());
            $this->tag->setDefault("usercreate", $marca->getUsercreate());
            $this->tag->setDefault("datecreate", $marca->getDatecreate());
            $this->tag->setDefault("userupdate", $marca->getUserupdate());
            $this->tag->setDefault("dateupdate", $marca->getDateupdate());
            
        }
    }

    /**
     * Creates a new marca
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "marca",
                "action" => "index"
            ));
        }

        $marca = new Marca();

        $marca->setNome($this->request->getPost("nome"));
        $marca->setUsercreate($this->request->getPost("usercreate"));
        $marca->setDatecreate($this->request->getPost("datecreate"));
        $marca->setUserupdate($this->request->getPost("userupdate"));
        $marca->setDateupdate($this->request->getPost("dateupdate"));
        

        if (!$marca->save()) {
            foreach ($marca->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "marca",
                "action" => "new"
            ));
        }

        $this->flash->success("marca was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "marca",
            "action" => "index"
        ));

    }

    /**
     * Saves a marca edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "marca",
                "action" => "index"
            ));
        }

        $id = $this->request->getPost("id");

        $marca = Marca::findFirstByid($id);
        if (!$marca) {
            $this->flash->error("marca does not exist " . $id);

            return $this->dispatcher->forward(array(
                "controller" => "marca",
                "action" => "index"
            ));
        }

        $marca->setNome($this->request->getPost("nome"));
        $marca->setUsercreate($this->request->getPost("usercreate"));
        $marca->setDatecreate($this->request->getPost("datecreate"));
        $marca->setUserupdate($this->request->getPost("userupdate"));
        $marca->setDateupdate($this->request->getPost("dateupdate"));
        

        if (!$marca->save()) {

            foreach ($marca->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "marca",
                "action" => "edit",
                "params" => array($marca->id)
            ));
        }

        $this->flash->success("marca was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "marca",
            "action" => "index"
        ));

    }

    /**
     * Deletes a marca
     *
     * @param string $id
     */
    public function deleteAction($id)
    {

        $marca = Marca::findFirstByid($id);
        if (!$marca) {
            $this->flash->error("marca was not found");

            return $this->dispatcher->forward(array(
                "controller" => "marca",
                "action" => "index"
            ));
        }

        if (!$marca->delete()) {

            foreach ($marca->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "marca",
                "action" => "search"
            ));
        }

        $this->flash->success("marca was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "marca",
            "action" => "index"
        ));
    }

}
