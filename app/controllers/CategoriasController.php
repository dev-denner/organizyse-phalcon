<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class CategoriasController extends ControllerBase
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for categorias
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Categorias", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id";

        $categorias = Categorias::find($parameters);
        if (count($categorias) == 0) {
            $this->flash->notice("The search did not find any categorias");

            return $this->dispatcher->forward(array(
                "controller" => "categorias",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $categorias,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a categoria
     *
     * @param string $id
     */
    public function editAction($id)
    {

        if (!$this->request->isPost()) {

            $categoria = Categorias::findFirstByid($id);
            if (!$categoria) {
                $this->flash->error("categoria was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "categorias",
                    "action" => "index"
                ));
            }

            $this->view->id = $categoria->id;

            $this->tag->setDefault("id", $categoria->getId());
            $this->tag->setDefault("descricao", $categoria->getDescricao());
            $this->tag->setDefault("parent", $categoria->getParent());
            $this->tag->setDefault("usercreate", $categoria->getUsercreate());
            $this->tag->setDefault("datacreate", $categoria->getDatacreate());
            $this->tag->setDefault("userupdate", $categoria->getUserupdate());
            $this->tag->setDefault("dataupdate", $categoria->getDataupdate());
            
        }
    }

    /**
     * Creates a new categoria
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "categorias",
                "action" => "index"
            ));
        }

        $categoria = new Categorias();

        $categoria->setId($this->request->getPost("id"));
        $categoria->setDescricao($this->request->getPost("descricao"));
        $categoria->setParent($this->request->getPost("parent"));
        $categoria->setUsercreate($this->request->getPost("usercreate"));
        $categoria->setDatacreate($this->request->getPost("datacreate"));
        $categoria->setUserupdate($this->request->getPost("userupdate"));
        $categoria->setDataupdate($this->request->getPost("dataupdate"));
        

        if (!$categoria->save()) {
            foreach ($categoria->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "categorias",
                "action" => "new"
            ));
        }

        $this->flash->success("categoria was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "categorias",
            "action" => "index"
        ));

    }

    /**
     * Saves a categoria edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "categorias",
                "action" => "index"
            ));
        }

        $id = $this->request->getPost("id");

        $categoria = Categorias::findFirstByid($id);
        if (!$categoria) {
            $this->flash->error("categoria does not exist " . $id);

            return $this->dispatcher->forward(array(
                "controller" => "categorias",
                "action" => "index"
            ));
        }

        $categoria->setId($this->request->getPost("id"));
        $categoria->setDescricao($this->request->getPost("descricao"));
        $categoria->setParent($this->request->getPost("parent"));
        $categoria->setUsercreate($this->request->getPost("usercreate"));
        $categoria->setDatacreate($this->request->getPost("datacreate"));
        $categoria->setUserupdate($this->request->getPost("userupdate"));
        $categoria->setDataupdate($this->request->getPost("dataupdate"));
        

        if (!$categoria->save()) {

            foreach ($categoria->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "categorias",
                "action" => "edit",
                "params" => array($categoria->id)
            ));
        }

        $this->flash->success("categoria was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "categorias",
            "action" => "index"
        ));

    }

    /**
     * Deletes a categoria
     *
     * @param string $id
     */
    public function deleteAction($id)
    {

        $categoria = Categorias::findFirstByid($id);
        if (!$categoria) {
            $this->flash->error("categoria was not found");

            return $this->dispatcher->forward(array(
                "controller" => "categorias",
                "action" => "index"
            ));
        }

        if (!$categoria->delete()) {

            foreach ($categoria->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "categorias",
                "action" => "search"
            ));
        }

        $this->flash->success("categoria was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "categorias",
            "action" => "index"
        ));
    }

}
