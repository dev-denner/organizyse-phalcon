<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class MovimentoBancarioController extends ControllerBase
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for movimento_bancario
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "MovimentoBancario", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id";

        $movimento_bancario = MovimentoBancario::find($parameters);
        if (count($movimento_bancario) == 0) {
            $this->flash->notice("The search did not find any movimento_bancario");

            return $this->dispatcher->forward(array(
                "controller" => "movimento_bancario",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $movimento_bancario,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a movimento_bancario
     *
     * @param string $id
     */
    public function editAction($id)
    {

        if (!$this->request->isPost()) {

            $movimento_bancario = MovimentoBancario::findFirstByid($id);
            if (!$movimento_bancario) {
                $this->flash->error("movimento_bancario was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "movimento_bancario",
                    "action" => "index"
                ));
            }

            $this->view->id = $movimento_bancario->id;

            $this->tag->setDefault("id", $movimento_bancario->getId());
            $this->tag->setDefault("data", $movimento_bancario->getData());
            $this->tag->setDefault("anocomp", $movimento_bancario->getAnocomp());
            $this->tag->setDefault("mescomp", $movimento_bancario->getMescomp());
            $this->tag->setDefault("historico", $movimento_bancario->getHistorico());
            $this->tag->setDefault("documento", $movimento_bancario->getDocumento());
            $this->tag->setDefault("credito", $movimento_bancario->getCredito());
            $this->tag->setDefault("debito", $movimento_bancario->getDebito());
            $this->tag->setDefault("motivo", $movimento_bancario->getMotivo());
            $this->tag->setDefault("usercreate", $movimento_bancario->getUsercreate());
            $this->tag->setDefault("datacreate", $movimento_bancario->getDatacreate());
            $this->tag->setDefault("userupdate", $movimento_bancario->getUserupdate());
            $this->tag->setDefault("dataupdate", $movimento_bancario->getDataupdate());
            
        }
    }

    /**
     * Creates a new movimento_bancario
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "movimento_bancario",
                "action" => "index"
            ));
        }

        $movimento_bancario = new MovimentoBancario();

        $movimento_bancario->setData($this->request->getPost("data"));
        $movimento_bancario->setAnocomp($this->request->getPost("anocomp"));
        $movimento_bancario->setMescomp($this->request->getPost("mescomp"));
        $movimento_bancario->setHistorico($this->request->getPost("historico"));
        $movimento_bancario->setDocumento($this->request->getPost("documento"));
        $movimento_bancario->setCredito($this->request->getPost("credito"));
        $movimento_bancario->setDebito($this->request->getPost("debito"));
        $movimento_bancario->setMotivo($this->request->getPost("motivo"));
        $movimento_bancario->setUsercreate($this->request->getPost("usercreate"));
        $movimento_bancario->setDatacreate($this->request->getPost("datacreate"));
        $movimento_bancario->setUserupdate($this->request->getPost("userupdate"));
        $movimento_bancario->setDataupdate($this->request->getPost("dataupdate"));
        

        if (!$movimento_bancario->save()) {
            foreach ($movimento_bancario->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "movimento_bancario",
                "action" => "new"
            ));
        }

        $this->flash->success("movimento_bancario was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "movimento_bancario",
            "action" => "index"
        ));

    }

    /**
     * Saves a movimento_bancario edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "movimento_bancario",
                "action" => "index"
            ));
        }

        $id = $this->request->getPost("id");

        $movimento_bancario = MovimentoBancario::findFirstByid($id);
        if (!$movimento_bancario) {
            $this->flash->error("movimento_bancario does not exist " . $id);

            return $this->dispatcher->forward(array(
                "controller" => "movimento_bancario",
                "action" => "index"
            ));
        }

        $movimento_bancario->setData($this->request->getPost("data"));
        $movimento_bancario->setAnocomp($this->request->getPost("anocomp"));
        $movimento_bancario->setMescomp($this->request->getPost("mescomp"));
        $movimento_bancario->setHistorico($this->request->getPost("historico"));
        $movimento_bancario->setDocumento($this->request->getPost("documento"));
        $movimento_bancario->setCredito($this->request->getPost("credito"));
        $movimento_bancario->setDebito($this->request->getPost("debito"));
        $movimento_bancario->setMotivo($this->request->getPost("motivo"));
        $movimento_bancario->setUsercreate($this->request->getPost("usercreate"));
        $movimento_bancario->setDatacreate($this->request->getPost("datacreate"));
        $movimento_bancario->setUserupdate($this->request->getPost("userupdate"));
        $movimento_bancario->setDataupdate($this->request->getPost("dataupdate"));
        

        if (!$movimento_bancario->save()) {

            foreach ($movimento_bancario->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "movimento_bancario",
                "action" => "edit",
                "params" => array($movimento_bancario->id)
            ));
        }

        $this->flash->success("movimento_bancario was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "movimento_bancario",
            "action" => "index"
        ));

    }

    /**
     * Deletes a movimento_bancario
     *
     * @param string $id
     */
    public function deleteAction($id)
    {

        $movimento_bancario = MovimentoBancario::findFirstByid($id);
        if (!$movimento_bancario) {
            $this->flash->error("movimento_bancario was not found");

            return $this->dispatcher->forward(array(
                "controller" => "movimento_bancario",
                "action" => "index"
            ));
        }

        if (!$movimento_bancario->delete()) {

            foreach ($movimento_bancario->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "movimento_bancario",
                "action" => "search"
            ));
        }

        $this->flash->success("movimento_bancario was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "movimento_bancario",
            "action" => "index"
        ));
    }

}
