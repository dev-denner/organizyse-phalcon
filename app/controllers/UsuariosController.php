<?php

use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class UsuariosController extends ControllerBase {

  public function initialize() {
    Phalcon\Tag::setTitle('Usuarios | Organizyse');
  }

  /**
   * Index action
   */
  public function indexAction() {
    $this->persistent->parameters = null;
  }

  /**
   * Searches for usuarios
   */
  public function searchAction() {

    $numberPage = 1;
    if ($this->request->isPost()) {
      $query = Criteria::fromInput($this->di, "Usuarios", $_POST);
      $this->persistent->parameters = $query->getParams();
    } else {
      $numberPage = $this->request->getQuery("page", "int");
    }

    $parameters = $this->persistent->parameters;
    if (!is_array($parameters)) {
      $parameters = array();
    }
    $parameters["order"] = "id";

    $usuarios = Usuarios::find($parameters);
    if (count($usuarios) == 0) {
      $this->flash->notice("The search did not find any usuarios");

      return $this->dispatcher->forward(array(
                  "controller" => "usuarios",
                  "action" => "index"
      ));
    }

    $paginator = new Paginator(array(
        "data" => $usuarios,
        "limit" => 10,
        "page" => $numberPage
    ));

    $this->view->page = $paginator->getPaginate();
  }

  /**
   * Displays the creation form
   */
  public function newAction() {
    
  }

  /**
   * Edits a usuario
   *
   * @param string $id
   */
  public function editAction($id) {

    if (!$this->request->isPost()) {

      $usuario = Usuarios::findFirstByid($id);
      if (!$usuario) {
        $this->flash->error("usuario was not found");

        return $this->dispatcher->forward(array(
                    "controller" => "usuarios",
                    "action" => "index"
        ));
      }

      $this->view->id = $id;

      $this->tag->setDefault("id", $usuario->getId());
      $this->tag->setDefault("login", $usuario->getLogin());
      $this->tag->setDefault("senha", $usuario->getSenha());
      $this->tag->setDefault("email", $usuario->getEmail());
      $this->tag->setDefault("nome", $usuario->getNome());
      $this->tag->setDefault("status", $usuario->getStatus());
      $this->tag->setDefault("token", $usuario->getToken());
    }
  }

  /**
   * Creates a new usuario
   */
  public function createAction() {

    if (!$this->request->isPost()) {
      return $this->dispatcher->forward(array(
                  "controller" => "usuarios",
                  "action" => "index"
      ));
    }

    $usuario = new Usuarios();

    $usuario->setLogin($this->request->getPost("login"));
    $usuario->setSenha($this->request->getPost("senha"));
    $usuario->setEmail($this->request->getPost("email", "email"));
    $usuario->setNome($this->request->getPost("nome"));
    $usuario->setStatus($this->request->getPost("status"));
    $usuario->setToken($this->request->getPost("token"));


    if (!$usuario->save()) {
      foreach ($usuario->getMessages() as $message) {
        $this->flash->error($message);
      }

      return $this->dispatcher->forward(array(
                  "controller" => "usuarios",
                  "action" => "new"
      ));
    }

    $this->flash->success("usuario was created successfully");

    return $this->dispatcher->forward(array(
                "controller" => "usuarios",
                "action" => "index"
    ));
  }

  /**
   * Saves a usuario edited
   *
   */
  public function saveAction() {

    if (!$this->request->isPost()) {
      return $this->dispatcher->forward(array(
                  "controller" => "usuarios",
                  "action" => "index"
      ));
    }

    $id = $this->request->getPost("id");

    $usuario = Usuarios::findFirstByid($id);
    if (!$usuario) {
      $this->flash->error("usuario does not exist " . $id);

      return $this->dispatcher->forward(array(
                  "controller" => "usuarios",
                  "action" => "index"
      ));
    }

    $usuario->setLogin($this->request->getPost("login"));
    $usuario->setSenha($this->request->getPost("senha"));
    $usuario->setEmail($this->request->getPost("email", "email"));
    $usuario->setNome($this->request->getPost("nome"));
    $usuario->setStatus($this->request->getPost("status"));
    $usuario->setToken($this->request->getPost("token"));


    if (!$usuario->save()) {

      foreach ($usuario->getMessages() as $message) {
        $this->flash->error($message);
      }

      return $this->dispatcher->forward(array(
                  "controller" => "usuarios",
                  "action" => "edit",
                  "params" => array($usuario->id)
      ));
    }

    $this->flash->success("usuario was updated successfully");

    return $this->dispatcher->forward(array(
                "controller" => "usuarios",
                "action" => "index"
    ));
  }

  /**
   * Deletes a usuario
   *
   * @param string $id
   */
  public function deleteAction($id) {

    $usuario = Usuarios::findFirstByid($id);
    if (!$usuario) {
      $this->flash->error("usuario was not found");

      return $this->dispatcher->forward(array(
                  "controller" => "usuarios",
                  "action" => "index"
      ));
    }

    if (!$usuario->delete()) {

      foreach ($usuario->getMessages() as $message) {
        $this->flash->error($message);
      }

      return $this->dispatcher->forward(array(
                  "controller" => "usuarios",
                  "action" => "search"
      ));
    }

    $this->flash->success("usuario was deleted successfully");

    return $this->dispatcher->forward(array(
                "controller" => "usuarios",
                "action" => "index"
    ));
  }

}
