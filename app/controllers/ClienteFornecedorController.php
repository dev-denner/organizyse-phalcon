<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class ClienteFornecedorController extends ControllerBase
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for cliente_fornecedor
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "ClienteFornecedor", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id";

        $cliente_fornecedor = ClienteFornecedor::find($parameters);
        if (count($cliente_fornecedor) == 0) {
            $this->flash->notice("The search did not find any cliente_fornecedor");

            return $this->dispatcher->forward(array(
                "controller" => "cliente_fornecedor",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $cliente_fornecedor,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a cliente_fornecedor
     *
     * @param string $id
     */
    public function editAction($id)
    {

        if (!$this->request->isPost()) {

            $cliente_fornecedor = ClienteFornecedor::findFirstByid($id);
            if (!$cliente_fornecedor) {
                $this->flash->error("cliente_fornecedor was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "cliente_fornecedor",
                    "action" => "index"
                ));
            }

            $this->view->id = $cliente_fornecedor->id;

            $this->tag->setDefault("id", $cliente_fornecedor->getId());
            $this->tag->setDefault("nome", $cliente_fornecedor->getNome());
            $this->tag->setDefault("usercreate", $cliente_fornecedor->getUsercreate());
            $this->tag->setDefault("datecreate", $cliente_fornecedor->getDatecreate());
            $this->tag->setDefault("userupdate", $cliente_fornecedor->getUserupdate());
            $this->tag->setDefault("dateupdate", $cliente_fornecedor->getDateupdate());
            
        }
    }

    /**
     * Creates a new cliente_fornecedor
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "cliente_fornecedor",
                "action" => "index"
            ));
        }

        $cliente_fornecedor = new ClienteFornecedor();

        $cliente_fornecedor->setNome($this->request->getPost("nome"));
        $cliente_fornecedor->setUsercreate($this->request->getPost("usercreate"));
        $cliente_fornecedor->setDatecreate($this->request->getPost("datecreate"));
        $cliente_fornecedor->setUserupdate($this->request->getPost("userupdate"));
        $cliente_fornecedor->setDateupdate($this->request->getPost("dateupdate"));
        

        if (!$cliente_fornecedor->save()) {
            foreach ($cliente_fornecedor->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "cliente_fornecedor",
                "action" => "new"
            ));
        }

        $this->flash->success("cliente_fornecedor was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "cliente_fornecedor",
            "action" => "index"
        ));

    }

    /**
     * Saves a cliente_fornecedor edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "cliente_fornecedor",
                "action" => "index"
            ));
        }

        $id = $this->request->getPost("id");

        $cliente_fornecedor = ClienteFornecedor::findFirstByid($id);
        if (!$cliente_fornecedor) {
            $this->flash->error("cliente_fornecedor does not exist " . $id);

            return $this->dispatcher->forward(array(
                "controller" => "cliente_fornecedor",
                "action" => "index"
            ));
        }

        $cliente_fornecedor->setNome($this->request->getPost("nome"));
        $cliente_fornecedor->setUsercreate($this->request->getPost("usercreate"));
        $cliente_fornecedor->setDatecreate($this->request->getPost("datecreate"));
        $cliente_fornecedor->setUserupdate($this->request->getPost("userupdate"));
        $cliente_fornecedor->setDateupdate($this->request->getPost("dateupdate"));
        

        if (!$cliente_fornecedor->save()) {

            foreach ($cliente_fornecedor->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "cliente_fornecedor",
                "action" => "edit",
                "params" => array($cliente_fornecedor->id)
            ));
        }

        $this->flash->success("cliente_fornecedor was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "cliente_fornecedor",
            "action" => "index"
        ));

    }

    /**
     * Deletes a cliente_fornecedor
     *
     * @param string $id
     */
    public function deleteAction($id)
    {

        $cliente_fornecedor = ClienteFornecedor::findFirstByid($id);
        if (!$cliente_fornecedor) {
            $this->flash->error("cliente_fornecedor was not found");

            return $this->dispatcher->forward(array(
                "controller" => "cliente_fornecedor",
                "action" => "index"
            ));
        }

        if (!$cliente_fornecedor->delete()) {

            foreach ($cliente_fornecedor->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "cliente_fornecedor",
                "action" => "search"
            ));
        }

        $this->flash->success("cliente_fornecedor was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "cliente_fornecedor",
            "action" => "index"
        ));
    }

}
