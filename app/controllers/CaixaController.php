<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class CaixaController extends ControllerBase
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for caixa
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Caixa", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id";

        $caixa = Caixa::find($parameters);
        if (count($caixa) == 0) {
            $this->flash->notice("The search did not find any caixa");

            return $this->dispatcher->forward(array(
                "controller" => "caixa",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $caixa,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a caixa
     *
     * @param string $id
     */
    public function editAction($id)
    {

        if (!$this->request->isPost()) {

            $caixa = Caixa::findFirstByid($id);
            if (!$caixa) {
                $this->flash->error("caixa was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "caixa",
                    "action" => "index"
                ));
            }

            $this->view->id = $caixa->id;

            $this->tag->setDefault("id", $caixa->getId());
            $this->tag->setDefault("descricao", $caixa->getDescricao());
            $this->tag->setDefault("saldo", $caixa->getSaldo());
            $this->tag->setDefault("usercreate", $caixa->getUsercreate());
            $this->tag->setDefault("datecreate", $caixa->getDatecreate());
            $this->tag->setDefault("userupdate", $caixa->getUserupdate());
            $this->tag->setDefault("dateupdate", $caixa->getDateupdate());
            
        }
    }

    /**
     * Creates a new caixa
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "caixa",
                "action" => "index"
            ));
        }

        $caixa = new Caixa();

        $caixa->setDescricao($this->request->getPost("descricao"));
        $caixa->setSaldo($this->request->getPost("saldo"));
        $caixa->setUsercreate($this->request->getPost("usercreate"));
        $caixa->setDatecreate($this->request->getPost("datecreate"));
        $caixa->setUserupdate($this->request->getPost("userupdate"));
        $caixa->setDateupdate($this->request->getPost("dateupdate"));
        

        if (!$caixa->save()) {
            foreach ($caixa->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "caixa",
                "action" => "new"
            ));
        }

        $this->flash->success("caixa was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "caixa",
            "action" => "index"
        ));

    }

    /**
     * Saves a caixa edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "caixa",
                "action" => "index"
            ));
        }

        $id = $this->request->getPost("id");

        $caixa = Caixa::findFirstByid($id);
        if (!$caixa) {
            $this->flash->error("caixa does not exist " . $id);

            return $this->dispatcher->forward(array(
                "controller" => "caixa",
                "action" => "index"
            ));
        }

        $caixa->setDescricao($this->request->getPost("descricao"));
        $caixa->setSaldo($this->request->getPost("saldo"));
        $caixa->setUsercreate($this->request->getPost("usercreate"));
        $caixa->setDatecreate($this->request->getPost("datecreate"));
        $caixa->setUserupdate($this->request->getPost("userupdate"));
        $caixa->setDateupdate($this->request->getPost("dateupdate"));
        

        if (!$caixa->save()) {

            foreach ($caixa->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "caixa",
                "action" => "edit",
                "params" => array($caixa->id)
            ));
        }

        $this->flash->success("caixa was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "caixa",
            "action" => "index"
        ));

    }

    /**
     * Deletes a caixa
     *
     * @param string $id
     */
    public function deleteAction($id)
    {

        $caixa = Caixa::findFirstByid($id);
        if (!$caixa) {
            $this->flash->error("caixa was not found");

            return $this->dispatcher->forward(array(
                "controller" => "caixa",
                "action" => "index"
            ));
        }

        if (!$caixa->delete()) {

            foreach ($caixa->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "caixa",
                "action" => "search"
            ));
        }

        $this->flash->success("caixa was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "caixa",
            "action" => "index"
        ));
    }

}
